

FPS = 10
OUT_LANE_RESET = 2 * FPS

class EpisodeCollector:

    def __init__(self, fps):
        self.fps = fps
        self.hours_per_step = 1.0 / fps / 60.0 / 60.0
        self.episodes = []
        self.valid_episodes = 0
        self.min_steps = 50
        self.next()

    def next(self):
        if len(self.episodes) > 0 and self.episodes[-1].steps >= self.min_steps:
            self.valid_episodes += 1
        self.episodes.append(EpisodeData())

    def step(self, py_measurements):
        self.episodes[-1].step(py_measurements)

    def results(self):
        data = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        total = 0.0
        for episode in self.episodes:
            # Ensure a valid episode
            if episode.steps < self.min_steps:
                continue

            total += 1
            ep_data = episode.metrics_csv_data()
            for i in range(len(data)):
                data[i] += ep_data[i]

        # Determine averages
        for i in range(len(data)):
            data[i] = data[i] / total
        return data

    def save_metrics_file(self, file):
        file.write(self.episodes[0].metrics_csv_header())
        for episode in self.episodes:
            if episode.steps >= self.min_steps:
                file.write("\n")
                file.write(",".join(str(x) for x in episode.metrics_csv_data()))
        file.write("\n")
        file.write(",".join(str(x) for x in self.results()))

    def save_crashes_file(self, file):
        file.write(self.episodes[0].crashes_csv_header())
        for episode in self.episodes:
            if episode.steps >= self.min_steps:
                crash = episode.crashes_csv_data()
                if crash is not None:
                    file.write("\n")
                    file.write(",".join(str(x) for x in crash))

    def save_out_of_lane_file(self, file):
        file.write(self.episodes[0].out_of_lane_csv_header())
        for episode in self.episodes:
            if episode.steps >= self.min_steps:
                instances = episode.out_of_lane_csv_data()
                for instance in instances:
                    file.write("\n")
                    file.write(",".join(str(x) for x in instance))

    def save_summary_file(self, file):
        total_steps = 0
        total_speed_counter = 0.0
        total_inlane_steps = 0
        total_reward = 0.0
        total_collisions = 0
        total_ool = 0
        for episode in self.episodes:
            if episode.steps >= self.min_steps:
                total_steps += episode.steps
                total_speed_counter += episode.speed_counter
                total_inlane_steps += episode.inlane_steps
                total_reward += episode.reward
                total_collisions += episode.collision
                total_ool += episode.out_of_lane_instances

        # Now do the math
        hours_driven = self.hours_per_step * total_steps
        avg_km_per_hr = total_speed_counter / total_steps
        km_traveled = avg_km_per_hr * hours_driven

        file.write("""
        total_steps: {steps}
        hours_driven: {hours}
        km_traveled: {km}
        average_km_per_hour: {speed}
        inlane_percent: {inlane}
        out_of_lane_per_km: {ool}
        collision_per_km: {collision}
        reward_per_km: {reward}
        """.format(hours=hours_driven,
                   steps=total_steps,
                   km=km_traveled,
                   speed=avg_km_per_hr,
                   inlane=total_inlane_steps / total_steps * 100.0,
                   ool=total_ool / km_traveled,
                   collision=total_collisions / km_traveled,
                   reward=total_reward / km_traveled))


class EpisodeData:

    def __init__(self):
        self.steps = 0
        self.speed_counter = 0.0
        self.inlane_steps = 0
        self.reward = 0.0
        self.collision = 0
        self.is_out_of_lane = False
        self.out_of_lane_instances = 0
        self.out_of_lane_cooldown = 0
        self.crash_location = None
        self.out_of_lane_locations = []

    def step(self, py_measurements):
        # Simple measurements
        self.steps += 1
        if py_measurements["collision_vehicles"] > 0 or py_measurements["collision_pedestrians"] > 0 or py_measurements["collision_other"] > 0:
            self.collision = 1
            self.crash_location = [py_measurements["x"], py_measurements["y"]]

        # Update speed and reward
        self.speed_counter += py_measurements["forward_speed"] * 3.6
        self.reward += py_measurements["reward"]

        # Out of lane counter
        is_out_of_lane = py_measurements["intersection_offroad"] > 0.05 \
                         or py_measurements["intersection_otherlane"] > 0.05
        if is_out_of_lane and self.out_of_lane_cooldown <= 0:
            self.out_of_lane_cooldown = OUT_LANE_RESET + 1
            self.out_of_lane_instances += 1
            self.out_of_lane_locations.append([py_measurements["x"], py_measurements["y"]])
        else:
            self.inlane_steps += 1

        # Reduce out of lane cooldown
        if self.out_of_lane_cooldown > 0:
            self.out_of_lane_cooldown -= 1
        self.is_out_of_lane = is_out_of_lane

    def metrics_csv_header(self):
        return "Steps,Speed Counter,Inlane Steps,OOL Instances,Collisions,Reward"

    def metrics_csv_data(self):
        return [
            self.steps,
            self.speed_counter,
            self.inlane_steps,
            self.out_of_lane_instances,
            self.collision,
            self.reward
        ]

    def crashes_csv_header(self):
        return "X,Y"

    def crashes_csv_data(self):
        return self.crash_location

    def out_of_lane_csv_header(self):
        return "X,Y"

    def out_of_lane_csv_data(self):
        return self.out_of_lane_locations
