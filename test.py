from carla_env.env import CarlaEnv
from done_error import DoneError
from example_collector import EpisodeCollector
import deepq_learner
from spec_files import ENV_LEVELS, DEEPQ_BASE, FILENAMES, use_deeplab, use_deeplab_mixed
from model_builder import model_function

class FinishedCompleter:
    def __init__(self, collector, deadzone=0):
        self.collector = collector
        self.deadzone = deadzone

    def on_next(self):
        self.collector.next()

    def on_step(self, py_measurements):
        if py_measurements["step"] > self.deadzone:
            self.collector.step(py_measurements)


def main():
    # Get environment
    config = ENV_LEVELS["test"]
    use_deeplab(config)
    # use_deeplab_mixed(config)
    env = CarlaEnv(config)

    collector = EpisodeCollector(config["fps"])
    completor = FinishedCompleter(collector, deadzone=config["episode_deadzone"])
    env.on_next = completor.on_next
    env.on_step = completor.on_step

    learn = deepq_learner.DeepqLearner(
        env=env,
        q_func=model_function(is_training=False),
        config=DEEPQ_BASE["test"]
    )

    print("Running testing....")
    try:
        learn.run()
    except DoneError:
        pass
    except Exception as e:
        print("Testing Failed!")
        raise e
    finally:
        print("Closing environment.")
        env.close()
        collector.next()

        # Determine results
        results = collector.results()
        print(",".join(str(x) for x in results))
        with open(FILENAMES["results"], 'w') as file:
            collector.save_metrics_file(file)
        with open(FILENAMES["crashes"], 'w') as file:
            collector.save_crashes_file(file)
        with open(FILENAMES["out-of-lanes"], 'w') as file:
            collector.save_out_of_lane_file(file)
        with open(FILENAMES["summary"], 'w') as file:
            collector.save_summary_file(file)


if __name__ == '__main__':
    main()
