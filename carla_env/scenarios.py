from utils.spec_file_reader import DEFAULT_DEADZONE

DEFAULT_STEPS = 3000 + DEFAULT_DEADZONE

TEST_WEATHERS = [0, 2, 5, 7, 9, 10, 11, 12, 13]
TRAIN_WEATHERS = [1, 3, 4, 6, 8, 14]

EVAL_WEATHERS = [1, 3, 4, 6, 8, 14,  # 6x train weather
                 0, 2, 5, 7, 9, 10]  # 6x test weather

def build_scenario(
        city, start, vehicles, pedestrians, max_steps, weathers):
    return {
        "city": city,
        "num_vehicles": vehicles,
        "num_pedestrians": pedestrians,
        "weather_distribution": weathers,
        "start_pos_id": start,
        "end_pos_id": start,
        "max_steps": max_steps,
    }

STARTS_TRAIN = [
    38, 4, 12, 62, 43, 64, 78, 59, 61, 35, 12, 0, 75, 54, 45,
    46, 53, 80, 65, 0, 54, 51, 16, 17, 77, 37, 8, 60, 38, 21,
    58, 74, 44, 71, 14, 34, 43, 75, 80, 3, 75, 50, 11, 77, 79,
    40, 58, 79, 16, 27
]

STARTS_TOWN1_EVAL = [
    26, 42, 46, 65, 76, 85, 110, 137, 138, 140,
    0, 7, 26, 33, 36, 39, 61, 95, 110, 147]

STARTS_TOWN2_EVAL = [
    8, 14, 21, 37, 44, 50, 58, 60, 71, 74,
    4, 12, 35, 38, 43, 59, 61, 62, 64, 78]

TOWN1_TRAIN = [
    build_scenario("Town01", start, 0, 0, DEFAULT_STEPS, TRAIN_WEATHERS)
    for start in STARTS_TRAIN]

TOWN1_EVAL = [
    build_scenario("Town01", pose, 0, 0, DEFAULT_STEPS, EVAL_WEATHERS)
    for pose in STARTS_TOWN1_EVAL
]

TOWN2_TRAIN = [
    build_scenario("Town02", start, 0, 0, DEFAULT_STEPS, TRAIN_WEATHERS)
    for start in STARTS_TRAIN]

TOWN2_EVAL = [
    build_scenario("Town02", start, 0, 0, DEFAULT_STEPS, EVAL_WEATHERS)
    for start in STARTS_TOWN2_EVAL
]