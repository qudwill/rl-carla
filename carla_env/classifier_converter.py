import numpy as np

BASE_CLASSIFICATIONS = 13
KEEP_CLASSES = [
    [0, 1, 2, 3, 4, 5, 8, 9, 10, 11, 12],  # 1 -> Misc
    [6],  # 2 -> Road lines
    [7],  # 3 -> Roads
]
KEEP_CLASSIFICATIONS = len(KEEP_CLASSES)

COLORS = [
    [255, 255, 255],  # None
    [0, 255, 0],  # Road Lines
    [0, 0, 255],  # Roads
]

# Reclassifier for GROUND TRUTH
CLASS_CONVERTER = {}
for idx, sublist in enumerate(KEEP_CLASSES):
    for subclass in sublist:
        CLASS_CONVERTER[subclass] = idx


def reclassifier(a):
    return CLASS_CONVERTER[a]


RECLASSIFIER = np.vectorize(reclassifier)


# Reclassifier for DEEPLAB
DL_CLASS_CONVERTER = {}
DL_CLASS_CONVERTER[0] = 0
DL_CLASS_CONVERTER[1] = 1
DL_CLASS_CONVERTER[2] = 2
DL_CLASS_CONVERTER[3] = 0


def dl_reclassifier(a):
    return DL_CLASS_CONVERTER[a]


DL_RECLASSIFIER = np.vectorize(dl_reclassifier)


# Fuse the data
def fuse_with_depth(classifications, depth, extra_layers=0):
    # Create the class array - set 1.0 for each pixel a given class is present
    shape = classifications.shape
    class_markers = np.full((shape[0], shape[1], KEEP_CLASSIFICATIONS + extra_layers), 1, dtype=np.float32)
    for x in range(shape[0]):
        for y in range(shape[1]):
            class_markers[x, y, classifications[x, y]] = depth[x, y]

    # Multiply depth information through the class markers to make fused observation frame
    # obs = class_markers * depth[:, :, np.newaxis]
    return class_markers


def depth_to_alpha(a):
    boosted = min(a * 15, 1.0)
    return pow(1.0 - boosted, 2)


DEPTH_TO_ALPHA = np.vectorize(depth_to_alpha)


def rgb_depth_image(class_data, depth):
    shape = (class_data.shape[0], class_data.shape[1], 3)

    # Determine alpha values
    alpha_matrix = DEPTH_TO_ALPHA(depth)

    # Determine the desired colors and place them on the image
    img = np.zeros(shape, dtype=np.uint8)
    for x in range(shape[0]):
        for y in range(shape[1]):
            colors = COLORS[class_data[x, y]]
            alpha = alpha_matrix[x, y]
            img[x, y, 0] = colors[0] * alpha
            img[x, y, 1] = colors[1] * alpha
            img[x, y, 2] = colors[2] * alpha

    # # Determine the depth -> alpha
    #
    #
    #
    # colorated = COLORATOR(fused_data)
    # for x in range(shape[0]):
    #     for y in range(shape[1]):
    #         for idx, value in enumerate(COLORS):
    #             color = value
    #             closeness = 1.0 - fused_data[x, y, idx]
    #             mult = pow(closeness, 2)
    #             # mult = 1.0 - min(0.01, depth) * 100
    #             img[x, y, 0] = max(img[x, y, 0], int(color[0] * mult))
    #             img[x, y, 1] = max(img[x, y, 1], int(color[1] * mult))
    #             img[x, y, 2] = max(img[x, y, 2], int(color[2] * mult))

    return img


def class_to_rgb(data):
    pass
