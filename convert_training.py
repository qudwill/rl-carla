import os

MEASUREMENTS = "/media/grant/SlowData/carla-experiments/exploration/blocks_train1/measurements"



def measurement_file_to_csv(file):
    csv_rows = []
    with open(file, 'r') as f:
        for line in f:
            obj = line
            step = obj["step"]
            if step > 100:
                reward = obj["reward"]
                speed = obj["forward_speed"] * 3.6
                csv_rows.append((step, reward, speed))

    # Return
    return csv_rows




# Iterate each file in order of date
for measurement_file in sorted(os.listdir(MEASUREMENTS)):
    print(measurement_file)

# Load up the json file
