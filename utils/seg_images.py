import math
import os
import random
import threading
from queue import Queue
import numpy as np
from PIL import Image


BREAKPOINT = 2500


CONVERSION = {
    0: 0,
    128: 1,
    255: 2
}


def color_converter(a):
    return CONVERSION[a]


COLOR_CONVERTER = np.vectorize(color_converter, otypes=[np.uint8])


def target_folder(base_folder, index):
    bp = math.floor((index - 1.0) / BREAKPOINT) + 1
    folder = '{folder}{bp:>04}'.format(folder=base_folder, bp=bp)
    if not os.path.exists(folder):
        os.mkdir(folder)
    return folder


def index_files(base_folder, index):
    folder = target_folder(base_folder, index)
    file_base = '{folder}/{index:>08}'.format(folder=folder, index=index)
    class_file = '{file_base}_class.png'.format(file_base=file_base)
    img_file = '{file_base}_img.png'.format(file_base=file_base)
    return img_file, class_file


def load_class_file(class_file):
    arr = np.array(Image.open(class_file))
    return COLOR_CONVERTER(arr)


def load_index(base_folder, index):
    # Determine file locations
    rgb_file, class_file = index_files(base_folder, index)

    # Load the files
    rgb_file_data = np.array(Image.open(rgb_file))
    class_file_data = np.array(Image.open(class_file))
    class_file_data = COLOR_CONVERTER(class_file_data)

    # Return both
    return rgb_file_data, class_file_data


def load_count_file(base_folder):
    count_file = base_folder + 'record_count.txt'
    with open(count_file) as f:
        return int(f.readline().strip())


class RandomizedIndexQueue:
    def __init__(self, count, randomize=True):
        self.count = count
        self.randomize = randomize
        self.to_visit = []
        self.visit_index = 0
        self.generate_queue()

    def generate_queue(self):
        self.to_visit = [i for i in range(1, self.count + 1)]
        if self.randomize:
            random.shuffle(self.to_visit)

    def next(self):
        index = self.to_visit[self.visit_index]
        self.visit_index += 1
        if self.visit_index >= self.count:
            self.visit_index = 0
            self.generate_queue()
        return index


class SegmentationFilenamesQueue:
    def __init__(self, folder):
        self.folder = folder
        count = load_count_file(folder)
        self.index_queue = RandomizedIndexQueue(count, randomize=False)

    def next(self):
        return index_files(self.folder, self.index_queue.next())

    def count(self):
        return self.index_queue.count


class RandomizedSegmentationQueue:
    def __init__(self, folder, randomize=True):
        self.folder = folder
        count = load_count_file(folder)
        self.index_queue = RandomizedIndexQueue(count, randomize)

    def next(self):
        return load_index(self.folder, self.index_queue.next())

    def count(self):
        return self.index_queue.count


class PreloadedSegmentationQueue:
    def __init__(self, folder, randomize=True, preload_count=100):
        self.queue = RandomizedSegmentationQueue(folder, randomize=randomize)
        self.preload_count = preload_count
        self.preloaded_queue = Queue(maxsize=preload_count)
        self.shutdown = False

        # Create the launch thread and start it
        def launch_thread():
            self.worker_thread()
        t = threading.Thread(target=launch_thread)
        t.start()

    def worker_thread(self):
        ''' Iterate until dead '''
        while not self.shutdown:
            # Load next and block until room in the queue
            next_item = self.queue.next()
            self.preloaded_queue.put(next_item, block=True, timeout=None)

    def next(self):
        ''' Returns the next item, blocking until it is found. '''
        return self.preloaded_queue.get(block=True, timeout=None)

    def clear_all_items(self):
        ''' Remove all items from the queue '''
        try:
            while True:
                self.preloaded_queue.get_nowait()
        except:
            pass

    def stop(self):
        ''' Clears the queue and stops the threads. '''
        self.shutdown = True
        self.clear_all_items()
