import tensorflow as tf

class DeepLabModel(object):
    """Class to load deeplab model and run inference."""

    INPUT_TENSOR_NAME = 'ImageTensor:0'
    OUTPUT_TENSOR_NAME = 'SemanticPredictions:0'
    INPUT_SIZE = 160

    def __init__(self, graph_file):
        """Creates and loads pretrained deeplab model."""
        print("Creating deeplab model...")
        self.graph = tf.Graph()
        with open(graph_file, "rb") as f:
            graph_def = tf.GraphDef.FromString(f.read())
        if graph_def is None:
            raise RuntimeError('Invalid inference graph.')

        with self.graph.as_default():
            tf.import_graph_def(graph_def, name='')

        # Create the session
        self.sess = tf.Session(
            graph=self.graph,
            config=tf.ConfigProto(
                gpu_options=tf.GPUOptions(per_process_gpu_memory_fraction=0.43)
            )
        )

    def run(self, image):
        """Runs inference on a single image.

        Args:
          image: A PIL.Image object, raw input image.

        Returns:
          seg_map: Segmentation map of `image`.
        """
        batch_seg_map = self.sess.run(
            self.OUTPUT_TENSOR_NAME,
            feed_dict={self.INPUT_TENSOR_NAME: [image]})
        seg_map = batch_seg_map[0]
        return seg_map
