import os

DEFAULT_DEADZONE = 100

DEEPQ_CONFIG_BASE = {
    "gpu_memory_fraction": 0.45,
    "lr": 1e-5,
    "max_timesteps": int(1e6),
    "buffer_size": int(3e4),
    "exploration_start_eps": 1.0,
    "exploration_fraction": 0.1,
    "exploration_final_eps": 0.1,
    "exploration_blocks": None,
    "train_freq": 6,
    "learning_starts": 30000,
    "target_network_update_freq": 3000,
    "batch_size": 32,
    "print_freq": 1,
    "checkpoint_freq": 1,
    "checkpoint_path": None,
    "gamma": 0.995,
    "prioritized_replay": True,
    "prioritized_replay_alpha": 0.6,
    "prioritized_replay_beta0": 0.4,
    "prioritized_replay_beta_iters": None,
    "prioritized_replay_eps": 1e-6,
    "param_noise": False,
    "episode_deadzone": DEFAULT_DEADZONE
}

# Default environment configuration
ENV_CONFIG = {
    # "server_binary": SERVER_BINARY,
    # "carla_out_path": CARLA_OUT_PATH,
    "measurements_subdir": "measurements",
    "save_images_rgb": False,
    "save_images_class": False,
    "save_images_fusion": False,
    "save_image_frequency": 1,
    "enable_planner": False,
    "deterministic_scenario_count": 0,  # Set to >0 if you want to loop through all scenarios X times then stop
    "framestack": 2,
    "convert_images_to_video": True,
    # TERM.EARLY_TERMINATIONS: [key for key in TERM.TERMINATION_FUNCTIONS.keys()],
    "verbose": True,
    "reward_function": "lane_keep",
    "render_x_res": 800,
    "render_y_res": 600,
    "x_res": 80,
    "y_res": 80,
    "server_map": "/Game/Maps/Town02",
    # "scenarios": [DEFAULT_SCENARIO],
    "squash_action_logits": False,
    "server_restart_interval": 50,
    "fps": 50,
    "quality": "Low",
    "segmenter_graph": None,
    "reward_noise": 0.01,
    "control_gamma": 0.85,  # This means about 200ms to fully change wheel to new position.
    "episode_deadzone": DEFAULT_DEADZONE
}


class ExperimentSpecFile:
    def __init__(self, params_folder, defaults_filename="defaults.yaml", params_filename="params.yaml", checkpoints="checkpoints"):
        self._params_folder = params_folder
        self.execution_parameters = {}

        # Find the default file
        default_params_file = self._resolve_file(defaults_filename, include_parent=True)
        if default_params_file is not None:
            print("Default params file location: " + default_params_file)
            self.load_params(default_params_file)
        else:
            print("No default parameters file found.")

        # Find the custom parameters
        exp_params_file = self._resolve_file(params_filename, include_parent=False)
        if exp_params_file is not None:
            print("Experiment params file location: " + exp_params_file)
            self.load_params(exp_params_file)
        else:
            print("No experiment parameters file found.")

        # Checkpoints
        self.checkpoints = self._resolve_file(checkpoints, include_parent=False)

    # First tries to get the file as provided.  If it doesn't exist, attempt to find the file
    # relative to the working folder.  If that fails, may try parent directory.
    # Returns resolved path if found, else None if not found
    def _resolve_file(self, filename, include_parent=False):
        path = filename
        if not os.path.exists(path):
            # Try local to folder
            path = os.path.join(self._params_folder, filename)
            if not os.path.exists(path):
                # Try parent folder
                if include_parent:
                    path = os.path.join(os.path.dirname(self._params_folder), filename)
                    if not os.path.exists(path):
                        path = None
                else:
                    path = None
        return path

    # Loads all parameters from the file
    def load_params(self, filename):
        with open(filename) as f:
            pass

    # Builds a ENV config and a DEEPQ config based on provided parameters
    def build_configs(self, execution):
        # Get config from the execution
        if execution not in self.execution_parameters:
            raise Exception("Could not find execution: " + execution)
        config = self.execution_parameters[execution]

        # Build the configs and return
        return self._build_env_config(config), self._build_deepq_config(config)

    def _build_deepq_config(self, config):
        deepq = DEEPQ_CONFIG_BASE.copy()
        deepq["checkpoint_path"] = self.checkpoints
        for key in deepq.keys():
            if key in config:
                deepq[key] = config[key]
        print("Deepq Configuration:\n" + str(deepq))
        return deepq

    def _build_env_config(self, config):
        env = ENV_CONFIG.copy()
        for key in env.keys():
            if key in config:
                env[key] = config[key]
        print("Env Configuration:\n" + str(env))
        return env
