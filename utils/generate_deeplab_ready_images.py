import tensorflow as tf
from tqdm import tqdm
import numpy as np
import math
import shutil
import utils.seg_images as si
from PIL import Image

import os, shutil



# SRC_FOLDER = '/media/grant/FastData/carla_saves/data-0.6.0/{type}-data/images/'.format(type=TYPE)

TYPE = 'train'
SRC_FOLDER = '/media/grant/SlowData/carla-final-data/deeplab/recordings/truthrun-train-200k/images/'

# TYPE = 'test'
# SRC_FOLDER = '/media/grant/SlowData/carla-final-data/deeplab/recordings/truthrun-test-50k/images/'

DST_FOLDER = '/media/grant/FastData/deeplab/segmentation/{type}/'.format(type=TYPE)


def process_index(index, class_file, image_file):
    # Determine files
    dst_img_file, dst_class_file = si.index_files(DST_FOLDER, index)

    # Copy the image file directly
    shutil.copyfile(image_file, dst_img_file)

    # Load up the class file
    class_file_data = si.load_class_file(class_file)
    class_file_data[0, 0] = 255
    Image.fromarray(class_file_data).save(dst_class_file)


def process_folder(index, folder):
    for class_filename in os.listdir(folder):
        class_file = folder + class_filename
        # Look for seed 'class' files
        if class_file.endswith('_class.png'):
            # Determine the rgb file
            image_file = class_file.replace('_class.png', '_rgb.png')
            if os.path.exists(image_file):
                process_index(index, class_file, image_file)
                index += 1
            else:
                print("Found: {}\nBut failed to find: {}".format(class_file, image_file))

    return index


if __name__ == '__main__':
    # Make the destination folder
    if not os.path.exists(DST_FOLDER):
        os.mkdir(DST_FOLDER)

    index = 1
    for episode_folder in os.listdir(SRC_FOLDER):
        print("Processing episode: {}".format(episode_folder))
        index = process_folder(index, SRC_FOLDER + episode_folder + "/")
        print("Episode completed... index at {}".format(index))

    print("Done!  Found {} files.".format(index - 1))

    with open(DST_FOLDER + "record_count.txt", 'w') as f:
        f.write("{}".format(index - 1))
